package com.epam.aws.training.mapper;

import com.epam.aws.training.dto.ImageMetadataDto;
import com.epam.aws.training.repository.model.ImageMetadata;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-14T16:14:55+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.14 (Oracle Corporation)"
)
@Component
public class ImageMetadataMapperImpl implements ImageMetadataMapper {

    @Override
    public ImageMetadataDto convertTo(ImageMetadata metadata) {
        if ( metadata == null ) {
            return null;
        }

        ImageMetadataDto imageMetadataDto = new ImageMetadataDto();

        imageMetadataDto.setSize( metadata.getSizeInBytes() );
        imageMetadataDto.setLastUpdateDate( metadata.getLastUpdateDate() );
        imageMetadataDto.setName( metadata.getName() );
        imageMetadataDto.setFileExtension( metadata.getFileExtension() );

        return imageMetadataDto;
    }

    @Override
    public List<ImageMetadataDto> convertTo(List<ImageMetadata> metadataList) {
        if ( metadataList == null ) {
            return null;
        }

        List<ImageMetadataDto> list = new ArrayList<ImageMetadataDto>( metadataList.size() );
        for ( ImageMetadata imageMetadata : metadataList ) {
            list.add( convertTo( imageMetadata ) );
        }

        return list;
    }
}
